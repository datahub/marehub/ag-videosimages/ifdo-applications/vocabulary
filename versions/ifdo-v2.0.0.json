{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://marine-imaging.com/fair/ifdo/schema-v2.0.0.json",
    "title": "image FAIR Digital Object",
    "description": "An iFDO file is a human- and machine-readable file format collecting metadata of an entire image set, without including the actual image data, only references to it through persistent identifiers.",
    "type": "object",
    "properties": {
        "image-set-name": {
            "description": "A unique name for the image set, should include `<image-project>, <image-event>, <image-sensor>` and optionally the purpose of imaging",
            "type": "string"
        },
	    "image-set-uuid": {
            "description": "A random UUID assigned to the entire image set",
            "type": "string",
            "format": "uuid"
        },
	    "image-set-handle": {
            "description": "A Handle URL (suggested: using the iumage-set-uuid) to point to the landing page of the data set",
            "type": "string",
            "format": "uri"
        },
	    "image-set-ifdo-version": {
            "description": "The semantic version information of the iFDO standard used.",
            "type": "string"
        },
        "image-datetime": {
            "description": "The fully-qualified ISO8601 UTC time of image acquisition (or start time of a video). E.g.: %Y-%m-%d %H:%M:%S.%f (in Python). You *may* specify a different date format using the optional iFDO capture field `image-datetime-format`",
            "type": "string",
            "format": "date-time"
        },
	    "image-latitude": {
            "description": "Y-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution)",
            "type": "number",
            "minimum": -90,
            "maximum": 90
        },
        "image-longitude": {
            "description": "X-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution)",
            "type": "number",
            "minimum": -180,
            "maximum": 180
        },
	    "image-altitude": {
            "description": "Z-coordinate of camera center in meters. Has negative values when camera is below sea level. Has positive values when the camera is above sea level.",
            "type": "number"
        },
	    "image-coordinate-reference-system": {
            "description": "The coordinate reference system, e.g. EPSG:4326",
            "type": "string"
        },
	    "image-coordinate-uncertainty-meters": {
            "description": "The average/static uncertainty of coordinates in this dataset, given in meters. Computed e.g. as the standard deviation of coordinate corrections during smoothing / splining.",
            "type": "number",
            "minimum": 0
        },
        "image-context": {
            "description": "The overarching project context whithin which the image set was created",
            "type": "string"
        },
	    "image-project": {
            "description": "The more specific project or expedition or cruise or experiment or ... whithin which the image set was created.",
            "type": "string"
        },
	    "image-event": {
            "description": "One event of a project or expedition or cruise or experiment or ... that led to the creation of this image set.",
            "type": "string"
        },
	    "image-platform": {
            "description": "A URI pointing to a description of the camera platform used to create this image set",
            "type": "string",
            "format": "uri"
        },
	    "image-sensor": {
            "description": "A URI pointing to a description of the sensor used to create this image set.",
            "type": "string",
            "format": "uri"
        },
	    "image-uuid": {
            "description": "A (random) UUID for the individual image file (still or moving). This UUID needs to be embedded within the image files.",
            "type": "string",
            "format": "uuid"
        },
	    "image-hash-sha256": {
            "description": "An SHA256 hash to represent the whole file (including UUID in file metadata header!) to verify integrity on disk",
            "type": "string"
        },
	    "image-pi": {
            "description": "Information to identify the principal investigator",
            "type": "object",
            "properties": {
                "$ref": "#/$defs/person"
            }
        },
	    "image-creators": {
            "description": "A list containing dicts for all creators containing: {orcid:..., name:...}",
            "type": "array",
            "items": {
                "$ref": "#/$defs/person"
            }
        },
        "image-license": {
            "description": "A URL pointing to the license to use the data (should be FAIR, e.g. **CC-BY** or CC-0)",
            "type": "string",
            "format": "uri"
        },
	    "image-copyright": {
            "description": "Copyright statement or contact person or office",
            "type": "string"
        },
        "image-abstract": {
            "description": "500 - 2000 characters describing what, when, where, why and how the data was collected. Includes general information on the event (aka station, experiment), e.g. overlap between images/frames, parameters on platform movement, aims, purpose of image capture etc.",
            "type": "string"
        },
        "image-entropy": {
		    "description": "Information content of an image / frame according to Shannon entropy.",
            "type": "number",
            "minimum": 0
        },
        "image-particle-count": {
            "description": "Counts of single particles/objects in an image / frame", 
            "type": "integer",
            "minimum": 0
        },
        "image-average-color": {
            "description": "The average colour for each image / frame and the `n` channels of an image (e.g. 3 for RGB)",
            "type": "array",
            "length": 3,
            "items": "integer"
        },
        "image-mpeg7-colorlayout": { 
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.",
            "type": "array",
            "items": "number"
        },
        "image-mpeg7-colorstatistic": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.",
            "type": "array",
            "items": "number"
        },
        "image-mpeg7-colorstructure": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.",
            "type": "array",
            "items": "number"
        },
        "image-mpeg7-dominantcolor": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.\nFormat: [float,float,...]"
        },
        "image-mpeg7-edgehistogram": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.\nFormat: [float,float,...]"
        },
        "image-mpeg7-homogeneoustexture": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.\nFormat: [float,float,...]"
        },
        "image-mpeg7-scalablecolor": {
            "description": "An nD feature vector per image / frame of varying dimensionality according to the chosen descriptor settings.\nFormat: [float,float,...]"
        },
        "image-annotation-labels": {
            "description": "All the labels used in the image-annotations. Specified by an id (e.g. AphiaID), a human-readable name and an optional description.\nFormat: [{id:`<LID>`,name:string,info:`<description>`},...]"
        },
        "image-annotation-creators": {
            "description": "All the annotators that created image-annotations. Specified by an id (e.g. ORCID), a human-readable name and an optional type specifying the annotator's expertise. Format: [{id:`<ORCID/UUID>`,name:string,type:string (expert, non-expert, AI)},...]"
        },
        "image-annotations": {
            "description": "This field is mighty powerful! It stores all annotations as a list of dictionaries of 3-4 fields: shape, coordinates, labels and (optional) frames. See further explanations below. The list of labels specifies the IDs or names of objects and annotators and their confidence. These should be specified in an `image-annotation-labels` and `image-annotation-creators` field (see above) to provide more information on the values used in these fields.\nFormat: [{coordinates:...,labels:...,shape:,...,frames:...},`<ANNOTATION-2>`,...]",
            "subFields":{
                    "shape":{"description": "The annotation shape is specified by a keyword (allowed values: see previous column).", "valid": ["single-pixel", "polyline", "polygon", "circle", "rectangle", "ellipse", "whole-image"]},
                    "coordinates":{"description": "The pixel coordinates of one annotation. The top-left corner of an image is the (0,0) coordinate. The x-axis is the horizontal axis. Pixel coordinates may be fractional. Coordinates can be given as a list of lists in case of video annotations that change coordinates over time (see `image-annotation:frames` below). Otherwise it is just a list of floats. The required number of pixel coordinates is defined by the shape (0 for whole-image, 2 for single-pixel, 3 for circle, 8 for ellipse/rectangle, 4 or more for polyline, 8 or more for polygon). The third coordinate value of a circle defines the radius. The first and last coordinates of a polygon must be equal.\nFormat: [[p1.x,p1.y,p2x,p2.y,...]..]"},
                    "labels":{"description": "A list of labels and annotators for one annotation. Use label IDs and annotator IDs here. The optional confidence in an annotation can be given as a float between 0 (100% uncertain) and 1 (100% certain). The confidence can be given independently for each label. Labels are independent of frames. The `created-at` field contains an ISO8601 string like `2011-10-05T14:48:00.000Z`\nFormat: [{label: `<LID>`, annotator: `<ORCID/UUID>`, created-at: `<datetime>`, confidence: `<float>`},...]"},
                    "frames":{"description": "(only required for video annotations) Frame times (in seconds from the beginning of a video) of a video annotation. Each frame time is linked to one entry in `image-annotations:coordinates` at the same position in the list, which specifies the current coordinates of the annotation at that frame.\nFormat: [f1,...]"
        },
        "image-acquisition": {
            "description": "photo: still images, video: moving images, slide: microscopy images / slide scans",
            "type": "string",
            "enum": ["photo", "video", "slide"]
        },
        "image-quality": {
            "description": "raw: straight from the sensor, processed: QA/QC'd, product: image data ready for interpretation", 
            "type": "string",
            "enum": ["raw", "processed", "product"]
        },
        "image-deployment":{
            "description": "mapping: planned path execution along 2-3 spatial axes, stationary: fixed spatial position, survey: planned path execution along free path, exploration: unplanned path execution, experiment: observation of manipulated environment, sampling: ex-situ imaging of samples taken by other method", 
            "enum": ["mapping", "stationary", "survey", "exploration", "experiment", "sampling"]
        },
        "image-navigation": {
            "description": "satellite: GPS/Galileo etc., beacon: USBL etc., transponder: LBL etc., reconstructed: position estimated from other measures like cable length and course over ground",
            "enum": ["satellite", "beacon", "transponder", "reconstructed"]
        },
        "image-scale-reference": {
            "description": "3D camera: the imaging system provides scale directly, calibrated camera: image data and additional external data like object distance provide scale together, laser marker: scale information is embedded in the visual data, optical flow: scale is computed from the relative movement of the images and the camera navigation data", 
            "enum": ["3D camera", "calibrated camera", "laser marker", "optical flow"]
        },
        "image-illumination": {
            "description": "sunlight: the scene is only illuminated by the sun, artificial light: the scene is only illuminated by artificial light, mixed light: both sunlight and artificial light illuminate the scene", 
            "enum": ["sunlight", "artificial light", "mixed light"]
        },
        "image-pixel-magnitude": {
            "description": "average size of one pixel of an image",
            "enum": ["km", "hm", "dam", "m", "cm", "mm", "µm"]
        },
        "image-marine-zone": {
            "description": "seafloor: images taken in/on/right above the seafloor, water column: images taken in the free water without the seafloor or the sea surface in sight, sea surface: images taken right below the sea surface, atmosphere: images taken outside of the water, laboratory: images taken ex-situ", 
            "enum": ["seafloor", "water column", "sea surface", "atmosphere", "laboratory"]
        },
        "image-spectral-resolution": {
            "description": "grayscale: single channel imagery, rgb: three channel imagery, multi-spectral: 4-10 channel imagery, hyper-spectral: 10+ channel imagery", 
            "enum": ["grayscale", "rgb", "multi-spectral", "hyper-spectral"]
        },
        "image-capture-mode": {
            "description": "whether the time points of image capture were systematic, human-truggered or both", 
            "enum": ["timer", "manual", "mixed"]
        },
        "image-fauna-attraction": {
            "description": "Allowed: none, baited, light", 
            "enum": ["none", "baited", "light"]
        },
        "image-area-square-meter": {
            "description": "The footprint of the entire image in square meters", 
            "type": "number",
            "exclusiveMinimum": 0
        },
        "image-meters-above-ground": {
            "description": "Distance of the camera to the seafloor in meters", 
            "type": "number"
        },
        "image-acquisition-settings":{
            "description": "All the information that is recorded by the camera in the EXIF, IPTC etc. As a dict. Includes ISO, aperture, etc.",
            "type": "object"
        },
        "image-camera-yaw-degrees": {
            "description": "Camera view yaw angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \"right-hand rule\". I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north.", 
            "type": "number"
        },
        "image-camera-pitch-degrees": {
            "description": "Camera view pitch angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \"right-hand rule\". I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north.", 
            "type": "number"
        },
        "image-camera-roll-degrees": {
            "description": "Camera view roll angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \"right-hand rule\". I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north.", 
            "type": "number"
        },
        "image-overlap-fraction": {
            "description": "The average overlap of two consecutive images i and j as the area images in both of the images (A_i * A_j) divided by the total area images by the two images (A_i + A_j - A_i * A_j): f = A_i * A_j / (A_i + A_j - A_i * A_j) -> 0 if no overlap. 1 if complete overlap",
            "type": "number"
        },
        "image-datetime-format": {
            "description": "A date time format string in Python notation (e.g. %Y-%m-%d %H:%M:%S.%f) to specify a different date format used throughout the iFDO file. The assumed default is the one in brackets. Make sure to reach second-accuracy with your date times!",
            "type": "string"
        },
        "image-camera-pose": {
            "description": "Information required to specify camera pose. For details on subfields see rows below.", 
            "type": "object",
            "properties": {
                "pose-utm-zone": {
                    "description": "The UTM zone number",
                    "type": "string"
                },
                "pose-utm-epsg": {
                    "description": "The EPSG code of the UTM zone",
                    "type": "string"
                },
                "pose-utm-east-north-up-meters": {
                    "description": "The position of the camera center in UTM coordinates.",
                    "type": "array",
                    "length": 3,
                    "items": "number"
                },
                "pose-absolute-orientation-utm-matrix": {
                    "description": "3x3 row-major float rotation matrix that transforms a direction in camera coordinates (x,y,z = right,down,line of sight) into a direction in UTM coordinates (x,y,z = easting,northing,up)}",
                    "type": "array",
                    "length": 9
                }
            }
        },
        "image-camera-housing-viewport": {
            "description": "Information on the camera pressure housing viewport (the glass). For details on subfields see rows below.",
            "type": "object",
            "properties":{
                "viewport-type": {
                    "description": "e.g.: flatport, domeport, other",
                    "type": "string"
                },
                "viewport-optical-density": {
                    "description": "Unit-less optical density number (1.0=vacuum)", 
                    "type": "number"
                },
                "viewport-thickness-millimeter": {
                    "description": "Thickness of viewport in millimeters",
                    "type": "number"
                },
                "viewport-extra-description": {
                    "description": "A textual description of the viewport used",
                    "type": "string"
                }
            }
        },
        "image-flatport-parameters": {
            "description": "Information required to specify the characteristics of a flatport camera housing. For details on subfields see rows below.", 
            "type": "object",
            "properties": {
                "flatport-lens-port-distance-millimeter": {
                    "description": "The distance between the front of the camera lens and the inner side of the housing viewport in millimeters.", 
                    "type": "number"
                },
                "flatport-interface-normal-direction": {
                    "description": "3D direction vector to specify how the view direction of the lens intersects with the viewport (unit-less, (0,0,1) is aligned)",
                    "type": "array",
                    "length": 3,
                    "items": "number"
                },
                "flatport-extra-description": {
                    "description": "A textual description of the flatport used", 
                    "type": "string"
                }
            }
        },
        "image-domeport-parameters": {
            "description": "Information required to specify the characteristics of a domeport camera housing. For details on subfields see rows below.", 
            "type": "object",
            "properties": {
                "domeport-outer-radius-millimeter": {
                    "description": "Outer radius of the domeport - the part that has contact with the water.", 
                    "type": "number"
                },
                "domeport-decentering-offset-xyz-millimeter": {
                    "description": "3D offset vector of the camera center from the domeport center in millimeters",
                    "type": "array",
                    "length": 3,
                    "items": "number"
                },
                "domeport-extra-description": {
                    "description": "A textual description of the domeport used",
                    "type": "string"
                }
            }
        },
        "image-camera-calibration-model": {
            "description": "Information required to specify the camera calibration model. For details on the subfields see rows below.", 
            "type": "object",
            "properties": {
                "calibration-model-type": {
                    "description": "e.g.: rectilinear air, rectilinear water, fisheye air, fisheye water, other"
                },
                "calibration-focal-length-xy-pixel": {
                    "description": "2D focal length in pixels\nFormat: [float, float]"
                },
                "calibration-principal-point-xy-pixel": {
                    "description": "2D principal point of the calibration in pixels (top left pixel center is 0,0, x right, y down)\nFormat: [float,float]"
                },
                "calibration-distortion-coefficients": {
                    "description": "rectilinear: k1, k2, p1, p2, k3, k4, k5, k6, fisheye: k1, k2, k3, k4", 
                    "type": "array"
                },
                "calibration-approximate-field-of-view-water-xy-degree": {
                    "description": "Proxy for pixel to meter conversion, and as backup\nFormat: [float, float]"
                },
                "calibration-model-extra-description": {
                    "description": "Explain model, or if lens parameters are in mm rather than in pixel", 
                    "type": "string"
                }
            }
        },
        "image-photometric-calibration": {
            "description": "Information required to specify the photometric calibration. For details on the subfields see rows below.", 
            "type": "object",
            "properties": {
                "photometric-sequence-white-balancing": {
                    "description": "A text on how white-balancing was done.", 
                    "type": "string"
                },
                "photometric-exposure-factor-RGB": {
                    "description": "RGB factors applied to this image, product of ISO, exposure time, relative white balance\nFormat: [float, float, float]"
                },
                "photometric-sequence-illumination-type": {
                    "description": "e.g. constant artificial, globally adapted artificial, individually varying light sources, sunlight, mixed)"
                },
                "photometric-sequence-illumination-description": {
                    "description": "A text on how the image sequence was illuminated", 
                    "type": "string"
                },
                "photometric-illumination-factor-RGB": {
                    "description": "RGB factors applied to artificial lights for this image\nFormat: [float, float, float]"
                },
                "photometric-water-properties-description": {
                    "description": "A text describing the photometric properties of the water within which the images were capture", 
                    "type": "string"
                }
            }
        },
        "image-objective": {
            "description": "A general translation of the aims and objectives of the study, as they pertain to biology and method scope. This should define the primary and secondary data to be measured and to what precision.", 
            "type": "string"
        },
        "image-target-environment": {
            "description": "A description, delineation, and definition of the habitat or environment of study, including boundaries of such", 
            "type": "string"
        },
        "image-target-timescale": {
            "description": "A description, delineation, and definition of the period, interval or temporal environment of the study.", 
            "type": "string"
        },
        "image-spatial-contraints": {
            "description": "A description / definition of the spatial extent of the study area (inside which the photographs were captured), including boundaries and reasons for constraints (e.g. scientific, practical)", 
            "type": "string"
        },
        "image-temporal-constraints": {
            "description": "A description / definition of the temporal extent, including boundaries and reasons for constraints (e.g. scientific, practical)", 
            "type": "string"
        },
        "image-time-synchronisation": {
            "description": "Synchronisation procedure and determined time offsets between camera recording values and UTC", 
            "type": "string"
        },
        "image-item-identification-scheme": {
            "description": "How the images file names are constructed. Should be like this `<project>_<event>_<sensor>_<date>_<time>.<ext>`", 
            "type": "string"
        },
        "image-curation-protocol": {
            "description": "A description of the image and metadata curation steps and results", 
            "type": "string"
        }
    },
    "required": ["image-set-name","image-set-uuid","image-set-handle","image-set-idfo-version"],
    "$defs": {
        "person": {
            "type": "object",
            "required": [ "name", "orcid" ],
            "properties": {
                "name": {
                    "type": "string",
                    "description": "The full name of the person"
                },
                "orcid": {
                    "description": "Full ORCID URI of principal investigator",
                    "type": "string",
                    "format": "uri"
                }
            }
        }
    }
}
      
   

